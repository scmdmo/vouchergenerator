# VOUCHER GENERATOR / GENERADOR DE TICKETS #

## Enunciado ##
### Task: ###

    Write a program in Java, which when run would generate a list of token/voucher codes based on a type of voucher specified and the number of vouchers to generate, using a web interface. 

### Requirements: ###   

    Any generator implementations are valid, but you have to use at least two different algorithms. Just updating the generator type or quantity in the web interface should be enough to get a different list of voucher codes.

    Please generate the program as a web application artifact with Maven for deployment on a web application server. It must also include at least one unit test.

### Example of input: ###

    [vouchers]
    quantity = 5
    algorithm = sequentialNumbers

### Output: ###

    000001
    000002
    000003
    000004
    000005

## Solución/Solution ##
La aplicación ha sido probada en un Tomcat 8 y en un Eclipse Jetty 9.
Para compilar desde maven se utilizará "mvn clean install -Dmaven.test.skip=true".
Los test han sido probados desde eclipse incorporando el directorio src/main/webapp/WEB-INF
al classloader para que se reconozca el contexto de Spring desde los mismos.

## --Arquitectura del proyecto-- ##
El proyecto se trata de una aplicación Web SPA (Simple page application) con comunicación asíncrona con el servidor mediante 
llamadas AJAX hacia Web Service de tipo Rest.
Para esto se ha utilizado Java+Spring MVC para crear los controladores rest y la lógica de Back-End.
En la vista se ha utilizado el framework javascript Jquery además de la librería Materialize para
estilizar la vista siguiendo el paradigma de diseño gráfico Material Desing promovido por Google.
Las librerías Js y CSS externas han sido cargadas usando repositorios externos CDN por lo que es necesario
la salida a internet en el cliente donde se ejecute la aplicación.
La aplicación se basa en el patrón MVC donde el backend sigue un paradigma orientado a Servicios.
Se sigue una arquitectura basada en tres capas, los controladores frontales "front", el servicio (capa intermedia) "service"
y la capa de negocio "back". Esta pensada para que el flujo de ejecución sea de capa superior a inferior
nunca volviendo a capas anterior o referenciando a la misma capa. Tan solo se permite la referencia a la misma capa
bajo la capa inferior "back".

Las distintas capas se componen de singletons utilizando la inyección de dependencias de Spring para su creación y referencia.
Para la lógica de negocio que implica la generación de tokens siguiendo distintos algoritmos se ha utilizado como base el 
patrón de comportamiento Strategy reajustandolo para que cada algoritmo sea instanciado una única vez como siglenton en 
el contexto de Spring.
También se englobaron dentro de una nueva clase que aporta metainformación de los algoritmos y que se recupera
desde el front-end para la inicialización de la SPA. Añadiendo nuevos algoritmos a este mapa del contexto de spring
tendríamos disponibles nuevos algoritmos de generación en la vista sin la necesidad de recompilar el proyecto.