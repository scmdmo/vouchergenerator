package service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import service.VoucherGeneratorService;
import service.dto.Algorithm;
import service.dto.Token;
import back.VoucherGeneratorBusiness;

public class VoucherGeneratorServiceImplTest {
	
	private VoucherGeneratorService service;
	private VoucherGeneratorBusiness business;
	
	public VoucherGeneratorServiceImplTest() {
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:config/service-context.xml");		
		service = context.getBean("VoucherGeneratorServiceImpl", VoucherGeneratorServiceImpl.class);
		business = context.getBean("VoucherGeneratorBusinessImpl", VoucherGeneratorBusiness.class);
	}
	
	@Test
	public void testInitPage() {
		System.out.println("TEST: testInitPage");
		List<Algorithm> res = service.initPage();
		assertNotNull(res);
		assertTrue(res.size()!=0);
		ArrayList<Integer> values = new ArrayList<Integer>(res.size());
		for (Algorithm alg:res){			
			assertNotNull(alg.getName());
			assertNotNull(alg.getDescription());
			assertFalse(values.contains(alg.getValue())); //no values repetidos
			values.add(alg.getValue());
		}
	}

	@Test
	public void testGenerateTokens() {
		System.out.println("TEST: testGenerateTokens");
		List<Algorithm> res = service.initPage();
		for(Integer quantity = 10; quantity<100; quantity+=10){
			for(Algorithm alg:res){
				List<Token> tokens = business.generateTokens(alg.getValue(), quantity);
				assertNotNull(tokens);
				assertTrue(tokens.size()==quantity);
			}
		}		
	}
}
