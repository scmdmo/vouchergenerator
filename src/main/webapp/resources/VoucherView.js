/**
 * Created by Domin on 06/03/2016.
 */
$(document).ready(function() {
    //inicializamos los combos material
    //$('select').material_select();
	initPageDataRequest();
    $('button.btnsubmit').click(requestTokens);
});

function loadCard(title,description){
    var $card = $('.card-algorithm');
    $card.find("span").text(title);
    $card.find("p").text(description);
    if($card.css("display")==="none")
        $card.show(1000);
}

function initPageDataRequest(params){
    $.getJSON("app/init", params, function(data,status,xhr){
        if(status==="success"){
            initAlgCombo(data);
        }else{
            alert("Error init page");
        }
    });
}

function initAlgCombo(data){
    var $combo = $('select');
    var dataObj = {};
    for(var i=0;i<data.length;i++){
        $combo.append("<option value="+data[i].value+
            ">"+data[i].name+"</option>");
        dataObj[data[i].value]= data[i];
    }
    $combo.material_select();
    $combo.change(function(){
        var option=$(this).val();
        loadCard(dataObj[option].name, dataObj[option].description);
    });
}

function requestTokens(){
    var params = {};
    var aux;
    if(aux=$("select").val())
        params.alg = aux;
    if((aux=$("#count").val()) && $.isNumeric(aux))
        params.c = aux;
    console.log("Params: "+JSON.stringify(params));
    doRequestTokens(params);
}

function doRequestTokens(params){
    $.getJSON("app/generate", params, function(data,status,xhr){
        if(status==="success"){
            show(data);
        }else{
          alert("Error retrieving tokens");
        }
    });
}

function show(tokensArray){
    var div = Math.floor(tokensArray.length/3);
    var rem = tokensArray.length%3;
    var $table=$("table tbody");
    $table.empty();
    var j=0;
    for(var i=0;i<div;i++){
        $table.append("<tr><td>"+tokensArray[j].value+
            "</td>\n<td>"+tokensArray[j+1].value+
            "</td>\n<td>"+tokensArray[j+2].value+"</td></tr>");
        j+=3;
    }
    if(rem>0) {
        $table.append("<tr><td></td>\n<td></td>\n<td></td></tr>");
        var $tdsvect =$table.children("tr:last").children();
        for (var i=0; i<rem; i++) {
            $($tdsvect[i]).text(tokensArray[j].value);
            j += 1;
        }
    }
}