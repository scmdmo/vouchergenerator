/**
 * 
 */
package back.generators.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import back.generators.TokenGenerateStrategy;
import service.dto.Token;

/**
 * @author Domin
 *
 */
public class UUIDGenerator implements TokenGenerateStrategy {

	@Override
	public List<Token> ejecutar(Integer num) {
		ArrayList<Token> tokens = new ArrayList<Token>(num);
		for(int i=0;i<num;i++){
			tokens.add(new Token(UUID.randomUUID().toString()));
		}
		return tokens;
	}
}
