package back.generators.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import back.generators.TokenGenerateStrategy;
import service.dto.Token;

public class RandomGenerator implements TokenGenerateStrategy {
	final int min = 0;
	final int max = 10000;
	
	@Override
	public List<Token> ejecutar(Integer num) {
		ArrayList<Token> res = new ArrayList<Token>();
		Random rand = new Random();    
		
		for(int i=0; i<num; i++){
			int randomNum = rand.nextInt((max - min) + 1) + min;
			res.add(new Token(String.valueOf(randomNum)));
		}
		return res;
	}	

}
