
package back.generators.impl;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import service.dto.Token;
import back.generators.TokenGenerateStrategy;

/**
 * @author Domin
 *
 */
public class DateBasedGenerator implements TokenGenerateStrategy {

	protected static final char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	@Override
	public List<Token> ejecutar(Integer num) {
		ArrayList<Token> res = new ArrayList<Token>();
		Date date = new Date();		
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");	
			
			for(int i=0; i<num; i++){			
				byte[] bytesOfMessage = (date.toString()+i).getBytes("UTF-8");
				byte[] thedigest = md.digest(bytesOfMessage);			
				res.add(new Token(bytesToHex(thedigest)));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return res;
	}
}
