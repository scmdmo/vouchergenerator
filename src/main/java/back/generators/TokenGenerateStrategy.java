/**
 * 
 */
package back.generators;

import java.util.List;

import service.dto.Token;

/**
 * @author Domin
 *
 */
public interface TokenGenerateStrategy {
	
	List<Token> ejecutar(Integer num);
}
