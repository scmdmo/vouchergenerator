/**
 * 
 */
package back.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import service.dto.Algorithm;
import service.dto.Token;
import back.VoucherGeneratorBusiness;

/**
 * @author Domin
 *
 */
@Component
public class VoucherGeneratorBusinessImpl implements VoucherGeneratorBusiness {
	
	private Map<Integer, Algorithm> generators; 
	
	public void setGenerators(Map<Integer, Algorithm> generators) {
		this.generators = generators;
	}

	@Override
	public List<Algorithm> initPage() {		
		return new ArrayList<Algorithm>(generators.values());
	}
	
	@Override
	public List<Token> generateTokens(Integer alg, Integer quantity) {			
		return generators.get(alg).getStrategy().ejecutar(quantity);
	}
}
