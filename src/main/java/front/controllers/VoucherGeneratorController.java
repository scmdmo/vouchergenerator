/**
 * 
 */
package front.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.VoucherGeneratorService;
import service.dto.Algorithm;
import service.dto.Token;

/**
 * @author Domin
 *
 */
@Controller
public class VoucherGeneratorController {

	protected final Log logger = LogFactory.getLog(getClass());
	
	private VoucherGeneratorService generatorService;
	
	public void setGeneratorService(VoucherGeneratorService generatorService) {
		this.generatorService = generatorService;
	}
	
		
	@RequestMapping(value = "/init", method = RequestMethod.GET,  produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public List<Algorithm> initPage(){
		return generatorService.initPage();
	}
	
	
	//Con required llega, Para usar el not @NotNull de java validations añadir : <groupId>javax.validation</groupId>	<artifactId>validation-api</artifactId>

	@RequestMapping(value = "/generate", method = RequestMethod.GET,  produces = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public List<Token> generateTokens(@RequestParam(name="alg",required=false) Integer alg,
			@RequestParam(name="c", required=false, defaultValue="10") Integer quantity, HttpServletResponse response) {
		logger.info("Generate token request alg:" + alg + " quantity:" + quantity);
		if(alg==null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}				
		List<Token> res = generatorService.generateTokens(alg, quantity);
		logger.info("Response " + res);
		return res;
	}
}
