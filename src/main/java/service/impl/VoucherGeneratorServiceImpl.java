package service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import back.VoucherGeneratorBusiness;
import service.VoucherGeneratorService;
import service.dto.Algorithm;
import service.dto.Token;

@Service
public class VoucherGeneratorServiceImpl implements VoucherGeneratorService {

	private VoucherGeneratorBusiness generatorBusiness;
	
	public void setGeneratorBusiness(VoucherGeneratorBusiness generatorBusiness) {
		this.generatorBusiness = generatorBusiness;
	}
	
	@Override
	public List<Algorithm> initPage(){
		return generatorBusiness.initPage();
	}
	
	@Override
	public List<Token> generateTokens(Integer alg, Integer quantity) {
		quantity=(quantity<0?0:quantity);
		return generatorBusiness.generateTokens(alg, quantity);
	}
}
