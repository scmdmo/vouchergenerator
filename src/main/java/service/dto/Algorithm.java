/**
 * 
 */
package service.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import back.generators.TokenGenerateStrategy;

/**
 * @author Domin
 *
 */
public class Algorithm implements Serializable {
	private static final long serialVersionUID = 3255375331473500127L;
	
	private Integer value;
	private String name;
	private String description;
	
	@JsonIgnore
	private TokenGenerateStrategy strategy;
	
	public Integer getValue() {
		return value;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public TokenGenerateStrategy getStrategy() {
		return strategy;
	}
	
	public void setStrategy(TokenGenerateStrategy strategy) {
		this.strategy = strategy;
	}	
}
