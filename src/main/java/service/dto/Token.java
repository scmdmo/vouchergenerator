/**
 * 
 */
package service.dto;

import java.io.Serializable;

/**
 * @author Domingo
 *
 */
public class Token implements Serializable{	
	private static final long serialVersionUID = -848397430136723701L;
	
	private String value;

	public Token(String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}	
}
