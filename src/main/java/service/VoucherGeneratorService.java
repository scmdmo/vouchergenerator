/**
 * 
 */
package service;

import java.util.List;

import service.dto.Algorithm;
import service.dto.Token;

/**
 * @author Domin
 *
 */
public interface VoucherGeneratorService {
	
	List<Algorithm> initPage();
	List<Token> generateTokens(Integer alg, Integer quantity);
}
